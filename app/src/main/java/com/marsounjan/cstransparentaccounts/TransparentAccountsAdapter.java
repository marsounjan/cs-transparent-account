package com.marsounjan.cstransparentaccounts;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marsounjan.cstransparentaccounts.databinding.ListItemTransparentAccountBinding;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;
import com.marsounjan.cstransparentaccounts.viewmodel.TransparentAccountItemViewModel;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountsAdapter extends RecyclerView.Adapter<TransparentAccountsAdapter.TransparentAccountViewHolder> {

    private List<TransparentAccount> transparentAccountsListResponse;
    private final PublishSubject<TransparentAccount> onClickSubject = PublishSubject.create();

    public TransparentAccountsAdapter() {

    }

    public void setTransparentAccountsListResponse(List<TransparentAccount> transparentAccountsListResponse) {
        this.transparentAccountsListResponse = transparentAccountsListResponse;
    }

    @Override
    public TransparentAccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemTransparentAccountBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_transparent_account,
                parent,
                false);
        return new TransparentAccountViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(TransparentAccountViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(transparentAccountsListResponse.get(position));
            }
        });
        holder.bindItem(transparentAccountsListResponse.get(position));
    }

    @Override
    public int getItemCount() {
        if(transparentAccountsListResponse != null){
            return transparentAccountsListResponse.size();
        } else {
            return 0;
        }
    }

    public Observable<TransparentAccount> getAccountClick(){
        return onClickSubject.asObservable();
    }

    public static class TransparentAccountViewHolder extends RecyclerView.ViewHolder {
        final ListItemTransparentAccountBinding binding;

        public TransparentAccountViewHolder(ListItemTransparentAccountBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindItem(TransparentAccount transparentAccount) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new TransparentAccountItemViewModel(itemView.getContext(), transparentAccount));
            } else {
                binding.getViewModel().setTransparentAccount(transparentAccount);
            }
        }
    }
}

