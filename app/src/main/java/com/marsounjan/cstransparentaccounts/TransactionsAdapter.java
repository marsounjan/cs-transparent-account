package com.marsounjan.cstransparentaccounts;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.marsounjan.cstransparentaccounts.databinding.ListItemTransactionBinding;
import com.marsounjan.cstransparentaccounts.model.Transaction;
import com.marsounjan.cstransparentaccounts.viewmodel.TransactionItemViewModel;

import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder> {

    private List<Transaction> transactionListResponse;

    public TransactionsAdapter() {

    }

    public void setTransactionListResponse(List<Transaction> transactionListResponse) {
        this.transactionListResponse = transactionListResponse;
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemTransactionBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_transaction,
                parent,
                false);
        return new TransactionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, final int position) {

        holder.bindItem(transactionListResponse.get(position));
    }

    @Override
    public int getItemCount() {
        if(transactionListResponse != null){
            return transactionListResponse.size();
        } else {
            return 0;
        }
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {
        final ListItemTransactionBinding binding;

        public TransactionViewHolder(ListItemTransactionBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindItem(Transaction transaction) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new TransactionItemViewModel(itemView.getContext(), transaction));
            } else {
                binding.getViewModel().setTransaction(transaction);
            }
        }
    }
}

