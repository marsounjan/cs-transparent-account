package com.marsounjan.cstransparentaccounts.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marsounjan.cstransparentaccounts.R;
import com.marsounjan.cstransparentaccounts.TransactionsAdapter;
import com.marsounjan.cstransparentaccounts.databinding.FragmentAccountTransactionsBinding;
import com.marsounjan.cstransparentaccounts.model.Transaction;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;
import com.marsounjan.cstransparentaccounts.viewmodel.AccountTransactionsViewModel;

import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class AccountTransactionsFragment extends Fragment implements AccountTransactionsViewModel.DataListener{

    private static final String EXTRA_ACCOUNT = "account";

    private FragmentAccountTransactionsBinding binding;
    private AccountTransactionsViewModel viewModel;
    private TransparentAccount account;

    public static AccountTransactionsFragment newInstance(TransparentAccount transparentAccount){
        AccountTransactionsFragment fragment = new AccountTransactionsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_ACCOUNT, transparentAccount);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        account = (TransparentAccount) getArguments().getSerializable(EXTRA_ACCOUNT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account_transactions, container, false);
        View mainView = binding.getRoot();

        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel = new AccountTransactionsViewModel(getActivity(), this, account);
        binding.setViewModel(viewModel);

        setupRecyclerView(binding.recyclerView);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.destroy();
    }

    @Override
    public void onDataUpdate(List<Transaction> newData) {
        TransactionsAdapter adapter =
                (TransactionsAdapter) binding.recyclerView.getAdapter();
        adapter.setTransactionListResponse(newData);
        adapter.notifyDataSetChanged();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        TransactionsAdapter adapter = new TransactionsAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }
}
