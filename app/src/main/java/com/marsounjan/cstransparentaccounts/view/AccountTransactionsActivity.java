package com.marsounjan.cstransparentaccounts.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.marsounjan.cstransparentaccounts.R;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;
import com.marsounjan.cstransparentaccounts.view.fragment.AccountTransactionsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class AccountTransactionsActivity extends AppCompatActivity {

    private final String FRAGMENT_TAG_HOME = "transactionsFragment";
    private static final String EXTRA_ACCOUNT = "account";

    @BindView(R.id.toolbar) Toolbar toolbar;

    private TransparentAccount transparentAccount;

    public static Intent newIntent(Context context, TransparentAccount transparentAccount){
        Intent intent = new Intent(context, AccountTransactionsActivity.class);
        intent.putExtra(EXTRA_ACCOUNT,transparentAccount);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        transparentAccount = (TransparentAccount) getIntent().getSerializableExtra(EXTRA_ACCOUNT);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(String.format(getString(R.string.activity_title_transactions), transparentAccount.getAccountNumberWithBankCode()));

        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag(FRAGMENT_TAG_HOME) == null){
            fm.beginTransaction().add(
                    R.id.main_container,
                    AccountTransactionsFragment.newInstance(transparentAccount),
                    FRAGMENT_TAG_HOME
            ).commit();
        }
    }

}
