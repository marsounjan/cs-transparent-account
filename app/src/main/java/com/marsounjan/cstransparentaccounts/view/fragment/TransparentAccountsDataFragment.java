package com.marsounjan.cstransparentaccounts.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.marsounjan.cstransparentaccounts.model.PageableAPICallResponse;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;

import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountsDataFragment extends Fragment{

    private List<TransparentAccount> accountList;
    private PageableAPICallResponse.PageableParams pageableParams;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        pageableParams = new PageableAPICallResponse.PageableParams();
    }

    public List<TransparentAccount> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<TransparentAccount> accountList) {
        this.accountList = accountList;
    }

    public PageableAPICallResponse.PageableParams getPageableParams() {
        return pageableParams;
    }

    public void setPageableParams(PageableAPICallResponse.PageableParams pageableParams) {
        this.pageableParams = pageableParams;
    }
}
