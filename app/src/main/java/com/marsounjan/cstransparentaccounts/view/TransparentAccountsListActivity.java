package com.marsounjan.cstransparentaccounts.view;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.marsounjan.cstransparentaccounts.R;
import com.marsounjan.cstransparentaccounts.view.fragment.TransparentAccountsDataFragment;
import com.marsounjan.cstransparentaccounts.view.fragment.TransparentAccountsListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransparentAccountsListActivity extends AppCompatActivity {

    private final String FRAGMENT_TAG_HOME = "home_fragment";
    public final String FRAGMENT_TAG_DATA = "data_fragment";

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.activity_title_accounts);

        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag(FRAGMENT_TAG_DATA) == null){
            fm.beginTransaction().add(new TransparentAccountsDataFragment(), FRAGMENT_TAG_DATA).commit();
        }

        if(fm.findFragmentByTag(FRAGMENT_TAG_HOME) == null){
            fm.beginTransaction().add(R.id.main_container, new TransparentAccountsListFragment(), FRAGMENT_TAG_HOME).commit();
        }
    }
}
