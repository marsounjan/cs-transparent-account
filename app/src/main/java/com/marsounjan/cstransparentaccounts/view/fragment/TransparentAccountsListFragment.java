package com.marsounjan.cstransparentaccounts.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marsounjan.cstransparentaccounts.R;
import com.marsounjan.cstransparentaccounts.TransparentAccountsAdapter;
import com.marsounjan.cstransparentaccounts.databinding.FragmentHomeBinding;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;
import com.marsounjan.cstransparentaccounts.viewmodel.TransparentAccountsListViewModel;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountsListFragment extends Fragment implements TransparentAccountsListViewModel.DataListener{

    public final String FRAGMENT_TAG_DATA = "data_fragment";

    private FragmentHomeBinding binding;
    private TransparentAccountsListViewModel transparentAccountsListViewModel;
    private WeakReference<TransparentAccountsDataFragment> dataFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View mainView = binding.getRoot();

        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        dataFragment = new WeakReference<>((TransparentAccountsDataFragment) fm.findFragmentByTag(FRAGMENT_TAG_DATA));

        transparentAccountsListViewModel = new TransparentAccountsListViewModel(getActivity(), this, dataFragment.get().getAccountList(), dataFragment.get().getPageableParams());
        binding.setViewModel(transparentAccountsListViewModel);

        setupRecyclerView(binding.recyclerView);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        transparentAccountsListViewModel.destroy();
    }

    @Override
    public void onDataUpdate(List<TransparentAccount> newData) {
        TransparentAccountsAdapter adapter =
                (TransparentAccountsAdapter) binding.recyclerView.getAdapter();
        adapter.setTransparentAccountsListResponse(newData);
        if(dataFragment.get() != null){
            dataFragment.get().setAccountList(newData);
        }
        adapter.notifyDataSetChanged();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        TransparentAccountsAdapter adapter = new TransparentAccountsAdapter();
        adapter.setTransparentAccountsListResponse(dataFragment.get().getAccountList());
        transparentAccountsListViewModel.setOnAccountClickedSubscription(adapter.getAccountClick());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

}
