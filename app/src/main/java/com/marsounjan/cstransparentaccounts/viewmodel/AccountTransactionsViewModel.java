package com.marsounjan.cstransparentaccounts.viewmodel;

import android.content.Context;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.marsounjan.cstransparentaccounts.CSTransparentAccountsApplication;
import com.marsounjan.cstransparentaccounts.model.CSAPIService;
import com.marsounjan.cstransparentaccounts.model.PageableAPICallResponse;
import com.marsounjan.cstransparentaccounts.model.Transaction;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class AccountTransactionsViewModel implements ViewModel{

    public ObservableInt progressVisibility;
    private Context context;
    private Subscription subscription;

    private TransparentAccount account;
    private DataListener dataListener;
    private PageableAPICallResponse.PageableParams transactionsListParams;

    public AccountTransactionsViewModel(Context context, DataListener dataListener, TransparentAccount account) {
        this.context = context;
        this.dataListener = dataListener;
        this.account = account;

        progressVisibility = new ObservableInt(View.INVISIBLE);

        transactionsListParams = new PageableAPICallResponse.PageableParams();

        loadTransactionsForAccount();
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
        dataListener = null;
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }

    private void loadTransactionsForAccount() {

        progressVisibility.set(View.VISIBLE);
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        CSTransparentAccountsApplication application = CSTransparentAccountsApplication.get(context);
        CSAPIService apiService = application.getCsAPIService();
        subscription = apiService.listTransactionsForTransparentAccount(account.getAccountNumber(), transactionsListParams.buildSearchParams())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<List<Transaction>>() {
                    @Override
                    public void onCompleted() {
                        progressVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onError(Throwable error) {
                        progressVisibility.set(View.INVISIBLE);
                        Toast.makeText(context,error.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<Transaction> transactions) {
                        if(dataListener != null){
                            dataListener.onDataUpdate(transactions);
                        }
                    }
                });
    }

    public interface DataListener {
        void onDataUpdate(List<Transaction> transactionListResponse);
    }
}

