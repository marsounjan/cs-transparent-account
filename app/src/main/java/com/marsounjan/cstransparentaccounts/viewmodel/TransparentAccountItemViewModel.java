package com.marsounjan.cstransparentaccounts.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.widget.TextView;

import com.marsounjan.cstransparentaccounts.model.TransparentAccount;

import java.text.DateFormat;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountItemViewModel extends BaseObservable implements ViewModel {

    private ObservableField<TransparentAccount> transparentAccount;
    private Context context;

    public TransparentAccountItemViewModel(Context context, TransparentAccount transparentAccount) {
        this.transparentAccount = new ObservableField<>();
        this.transparentAccount.set(transparentAccount);
        this.context = context;
    }

    @BindingAdapter({"accNumBankCodeFormatted"})
    public static void setAccNumBankCodeFormatted(TextView view, TransparentAccount account) {
        view.setText(account.getAccountNumberWithBankCode());
    }

    @BindingAdapter({"accountBalanceWithCurrency"})
    public static void setAccountBalanceWithCurrency(TextView view, TransparentAccount account) {
        view.setText(account.getFormattedBalanceWithCurrency());
    }

    @BindingAdapter({"accountTransparencySince"})
    public static void setAccountTransparencySince(TextView view, TransparentAccount account) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        view.setText(df.format(account.getTransparencyFrom()));
    }

    @BindingAdapter({"accountTransparencyTo"})
    public static void setAccountTransparencyTo(TextView view, TransparentAccount account) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        view.setText(df.format(account.getTransparencyTo()));
    }

    @BindingAdapter({"accountUpdated"})
    public static void setAccountUpdated(TextView view, TransparentAccount account) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
        view.setText(df.format(account.getUpdated()));
    }

    public ObservableField<TransparentAccount> getTransparentAccount() {
        return transparentAccount;
    }

    public void setTransparentAccount(TransparentAccount transparentAccount) {
        this.transparentAccount.set(transparentAccount);
        notifyChange();
    }

    @Override
    public void destroy() {
        //In this case destroy doesn't need to do anything because there is not async calls
    }
}

