package com.marsounjan.cstransparentaccounts.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.marsounjan.cstransparentaccounts.CSTransparentAccountsApplication;
import com.marsounjan.cstransparentaccounts.model.CSAPIService;
import com.marsounjan.cstransparentaccounts.model.PageableAPICallResponse;
import com.marsounjan.cstransparentaccounts.model.TransparentAccount;
import com.marsounjan.cstransparentaccounts.model.TransparentAccountsListResponse;
import com.marsounjan.cstransparentaccounts.view.AccountTransactionsActivity;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountsListViewModel implements ViewModel{

    public ObservableInt progressVisibility;
    private Context context;
    private Subscription subscription;
    private Subscription onAccountClickedSubscription;

    private DataListener dataListener;
    private PageableAPICallResponse.PageableParams pageableAPICallParams;

    public TransparentAccountsListViewModel(
            Context context,
            DataListener dataListener,
            List<TransparentAccount> accountList,
            PageableAPICallResponse.PageableParams pageableParams
    ) {
        this.context = context;
        this.dataListener = dataListener;
        progressVisibility = new ObservableInt(View.INVISIBLE);
        pageableAPICallParams = pageableParams;

        if(accountList == null){
            loadTransparentAccounts();
        }
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        if (onAccountClickedSubscription != null && !onAccountClickedSubscription.isUnsubscribed()) onAccountClickedSubscription.unsubscribe();
        onAccountClickedSubscription = null;
        context = null;
        dataListener = null;
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }

    private void loadTransparentAccounts() {

        progressVisibility.set(View.VISIBLE);
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        CSTransparentAccountsApplication application = CSTransparentAccountsApplication.get(context);
        CSAPIService apiService = application.getCsAPIService();
        subscription = apiService.listTransparentAccounts(pageableAPICallParams.buildSearchParams())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<List<TransparentAccount>>() {
                    @Override
                    public void onCompleted() {
                        progressVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onError(Throwable error) {
                        progressVisibility.set(View.INVISIBLE);
                        Toast.makeText(context,error.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<TransparentAccount> weather) {
                        if(dataListener != null){
                            dataListener.onDataUpdate(weather);
                        }
                    }
                });
    }

    public void setOnAccountClickedSubscription(Observable<TransparentAccount> accountObservableField){
        if (onAccountClickedSubscription != null && !onAccountClickedSubscription.isUnsubscribed()) onAccountClickedSubscription.unsubscribe();
        onAccountClickedSubscription = accountObservableField.subscribe(new Subscriber<TransparentAccount>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(TransparentAccount account) {
                context.startActivity(AccountTransactionsActivity.newIntent(context, account));
            }
        });
    }

    public interface DataListener {
        void onDataUpdate(List<TransparentAccount> transparentAccountsListResponse);
    }
}
