package com.marsounjan.cstransparentaccounts.viewmodel;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public interface ViewModel {
    void destroy();
}
