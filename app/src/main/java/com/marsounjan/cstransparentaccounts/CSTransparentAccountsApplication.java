package com.marsounjan.cstransparentaccounts;

import android.app.Application;
import android.content.Context;

import com.marsounjan.cstransparentaccounts.model.CSAPIService;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class CSTransparentAccountsApplication extends Application {

    private CSAPIService csAPIService;
    private Scheduler defaultSubscribeScheduler;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static CSTransparentAccountsApplication get(Context context) {
        return (CSTransparentAccountsApplication) context.getApplicationContext();
    }

    public CSAPIService getCsAPIService() {
        if (csAPIService == null) {
            csAPIService = CSAPIService.Factory.create();
        }
        return csAPIService;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }
}
