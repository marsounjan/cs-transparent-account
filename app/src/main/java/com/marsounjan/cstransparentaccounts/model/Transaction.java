package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */

/*
{
      "amount": {
        "value": -92,
        "precision": 0,
        "currency": "CZK"
      },
      "type": "60000",
      "dueDate": "2016-08-31T00:00:00",
      "processingDate": "2016-08-31T00:00:00",
      "sender": {
        "accountNumber": "000000-0000000000",
        "bankCode": "0800",
        "iban": "CZ13 0800 0000 0029 0647 8309",
        "specificSymbol": "0000000000",
        "specificSymbolParty": "0000000000",
        "constantSymbol": "0158",
        "description": "(01.08.2016 - 31.08.2016)"
      },
      "receiver": {
        "accountNumber": "000000-2906478309",
        "bankCode": "0800",
        "iban": "CZ13 0800 0000 0029 0647 8309"
      }
    }
 */

public class Transaction {

    @Expose @SerializedName("amount") private Amount amount;
    @Expose @SerializedName("type") private String type;
    @Expose @SerializedName("dueDate") private String dueDate;
    @Expose @SerializedName("processingDate") private String processingDate;
    @Expose @SerializedName("sender") private TransparentAccount sender;
    @Expose @SerializedName("receiver") private TransparentAccount receiver;

    public Amount getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getDueDate() {
        return dueDate;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public TransparentAccount getSender() {
        return sender;
    }

    public TransparentAccount getReceiver() {
        return receiver;
    }

    public static class Amount {

        @Expose @SerializedName("value") private Double value;
        @Expose @SerializedName("precision") private Integer precision;
        @Expose @SerializedName("currency") private String currency;

        public Double getValue() {
            return value;
        }

        public Integer getPrecision() {
            return precision;
        }

        public String getCurrency() {
            return currency;
        }
    }

}
