package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public interface CSAPIService {

    @GET("transparentAccounts/accounts")
    Observable<List<TransparentAccount>> listTransparentAccounts(@QueryMap Map<String, String> params);

    @GET("transparentAccounts/transactions/{accountID}/")
    Observable<List<Transaction>> listTransactionsForTransparentAccount(@Path("accountID") String accountID, @QueryMap Map<String, String> params);

    class Factory {
        public static CSAPIService create() {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addNetworkInterceptor(interceptor);

            //add cs api token for every request
            httpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request request = chain.request();
                    request = request.newBuilder()
                            .header("WEB-API-Key", "737132af-2017-418b-ac5f-32aa924e3d0f")
                            .method(request.method(), request.body())
                            .build();
                    return chain.proceed(request);
                }
            });

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.csas.cz/sandbox/webapi/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(httpClientBuilder.build())
                    .build();

            return retrofit.create(CSAPIService.class);
        }
    }
}
