package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */

/**
 *
 * {
 "pageNumber": 0,
 "pageCount": 1,
 "pageSize": 50,
 "recordCount": 1,
 "accounts": [{}]
 }
 *
 */

public class PageableAPICallResponse {

    @Expose @SerializedName("pageNumber") private Integer pageNumber;
    @Expose @SerializedName("pageCount") private Integer pageCount;
    @Expose @SerializedName("pageSize") private Integer pageSize;
    @Expose @SerializedName("recordCount") private Integer recordCount;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getRecordCount() {
        return recordCount;
    }

    public static class PageableParams {

        private final int DEFAULT_PAGE_SIZE = 20;

        private Integer pageNum;
        private Integer pageSize = DEFAULT_PAGE_SIZE;

        public PageableParams() {
        }

        public Integer getPageNum() {
            return pageNum;
        }

        public void setPageNum(Integer pageNum) {
            this.pageNum = pageNum;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public void increasePageSizeByDefaultNumber(){
            this.pageSize = pageSize + DEFAULT_PAGE_SIZE;
        }

        public Map<String, String> buildSearchParams(){
            Map<String, String> params = new HashMap<>();
            if(pageNum != null){params.put("page", String.valueOf(pageNum));}
            if(pageSize != null){params.put("size", String.valueOf(pageSize));}
            return params;
        }
    }

}
