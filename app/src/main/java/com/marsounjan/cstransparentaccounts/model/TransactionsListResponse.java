package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransactionsListResponse extends PageableAPICallResponse{

    @Expose @SerializedName("transactions") private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        return transactions;
    }
}
