package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccountsListResponse extends PageableAPICallResponse{

    @Expose @SerializedName("accounts") private List<TransparentAccount> accounts;

    public List<TransparentAccount> getAccounts() {
        return accounts;
    }
}
