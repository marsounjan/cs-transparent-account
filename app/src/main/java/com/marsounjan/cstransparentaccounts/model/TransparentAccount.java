package com.marsounjan.cstransparentaccounts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 15.09.16.
 */
public class TransparentAccount implements Serializable {

    private final String DATE_FORMAT = "yyyy-MM-dd";

    @Expose @SerializedName("accNumber") private String accountNumber;
    @Expose @SerializedName("accBankCode") private String bankCode;
    @Expose @SerializedName("transparencyFrom") private String transparencyFrom;
    @Expose @SerializedName("transparencyTo") private String transparencyTo;
    @Expose @SerializedName("publicationTo") private String publicationTo;
    @Expose @SerializedName("actualizationDate") private Long actualizationDate;
    @Expose @SerializedName("balance") private Double balance;
    @Expose @SerializedName("currency") private String currency;
    @Expose @SerializedName("iban") private String iban;

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public String getPublicationTo() {
        return publicationTo;
    }

    public Long getActualizationDate() {
        return actualizationDate;
    }

    public Double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }

    public String getIban() {
        return iban;
    }

    public Date getTransparencyFrom(){
        if(transparencyFrom == null){
            return null;
        } else {
            return parseDate(transparencyFrom);
        }
    }

    public Date getTransparencyTo(){
        if(transparencyTo == null){
            return null;
        } else {
            return parseDate(transparencyTo);
        }
    }

    public Date getUpdated(){
        if(actualizationDate == null){
            return null;
        } else {
            return new Date(actualizationDate);
        }
    }

    public String getAccountNumberWithBankCode(){
        return String.valueOf(accountNumber) + "/" + String.valueOf(bankCode);
    }

    public String getFormattedBalanceWithCurrency(){
        if(balance == null || currency == null){
            return null;
        }
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance(currency));
        try {
            return format.format(balance);
        } catch(IllegalArgumentException e){
            return null;
        }
    }

    private Date parseDate(String dateInString){
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        try {
            return format.parse(dateInString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
